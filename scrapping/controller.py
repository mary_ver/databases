from addresses import *
from products import *
from models import *
from database import Database
from view import View


class Controller:
    option = -1
    database = Database()
    view = View()

    def start(self):
        self.database.connect()
        self.view.into()

    def stop(self):
        self.database.close()
        self.view.outro()

    def get_products(self):
        products = []
        # Chocolates
        scrap_prod("https://citymarket.zakaz.ua/en/categories/chocolate-citymarket/", 4, products)
        # Confectionary
        scrap_prod("https://citymarket.zakaz.ua/en/categories/confectionery-citymarket/", 12, products)
        # Wine
        scrap_prod("https://citymarket.zakaz.ua/en/categories/wine-citymarket/", 31, products)
        # Cheeses
        scrap_prod("https://citymarket.zakaz.ua/en/categories/cheeses-citymarket/", 7, products)
        # Yogurts
        scrap_prod("https://citymarket.zakaz.ua/en/categories/yogurt-citymarket/", 6, products)
        # Breakfast cereals and porridge
        scrap_prod("https://citymarket.zakaz.ua/en/categories/breakfast-cereals-and-porridge-citymarket/", 5, products)
        # Canned foods
        scrap_prod("https://citymarket.zakaz.ua/en/categories/canned-food-citymarket/", 10, products)
        for prod in products:
            prod.id = 0
            self.database.add_product(prod)

    def get_addresses(self):
        addresses = scrap_add()
        for addr in addresses:
            store = Store(0, addr)
            self.database.add_store(store)

    def gen_sales(self, n):
        self.database.generate_sales(n)

    def gen_prices(self, n):
        self.database.generate_prices(n)


