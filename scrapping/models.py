from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.dialects.postgresql import *
from sqlalchemy.orm import relationship

Base = declarative_base()

meta = MetaData()


class Product(Base):
    __tablename__ = 'products'
    id = Column(Integer, Sequence('products_id_seq'), primary_key=True, server_default=Sequence('products_id_seq')
                .next_value())
    name = Column(Text, nullable=false)
    category = Column(Text, nullable=false)
    sales = relationship("Sale")
    prices = relationship("Price")

    def __init__(self, id, name, category):
        self.id = int(id)
        self.name = name
        self.category = category


class Store(Base):
    __tablename__ = 'stores'
    id = Column(Integer, Sequence('stores_id_seq'), primary_key=True, server_default=Sequence('stores_id_seq')
                .next_value())
    address = Column(Text, nullable=False, unique=True)
    sales = relationship("Sale")

    def __init__(self, id, address):
        self.id = int(id)
        self.address = address


class Sale(Base):
    __tablename__ = 'sales'
    id = Column(Integer, Sequence('sales_id_seq'), primary_key=True, server_default=Sequence('sales_id_seq')
                .next_value())
    store_id = Column(Integer, ForeignKey('stores.id'))
    product_id = Column(Integer, ForeignKey('products.id'))
    amount = Column(Integer, nullable=false)
    date = Column(Date, nullable=false)

    def __init__(self, id, store_id, product_id, amount, date):
        self.id = int(id)
        self.store_id = int(store_id)
        self.product_id = int(product_id)
        self.amount = int(amount)
        self.date = date


class Price(Base):
    __tablename__ = 'prices'
    id = Column(Integer, Sequence('prices_id_seq'), primary_key=True, server_default=Sequence('prices_id_seq')
                .next_value())
    product_id = Column(Integer, ForeignKey('products.id'))
    price = Column(MONEY, nullable=false)
    date = Column(Date, nullable=false)

    def __init__(self, id, product_id, price, date):
        self.id = int(id)
        self.product_id = int(product_id)
        self.price = price
        self.date = date





