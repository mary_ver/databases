from controller import Controller


def main():
    controller = Controller()
    controller.start()
    controller.view.options()
    controller.option = controller.view.get_option()
    while controller.option != 0:
        if controller.option == 1:
            controller.get_products()
        elif controller.option == 2:
            controller.get_addresses()
        elif controller.option == 3:
            controller.option = controller.view.chose_table_gen()
            if controller.option is None:
                continue
            elif controller.option == 1:
                n = controller.view.choose_number()
                if n is None:
                    continue
                else:
                    controller.gen_sales(n)
            elif controller.option == 2:
                n = controller.view.choose_number()
                if n is None:
                    continue
                else:
                    controller.gen_prices(n)
            else:
                controller.view.incorrect_opt()
        else:
            controller.view.incorrect_opt()
        controller.view.options()
        controller.option = controller.view.get_option()


if __name__ == '__main__':
    main()
