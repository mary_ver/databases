import psycopg2
from sqlalchemy import *
from sqlalchemy.sql import text
from sqlalchemy.orm import sessionmaker
from models import *

DATABASE_URI = 'postgres+psycopg2://postgres:qwerty@localhost:5432/store'
engine = create_engine(DATABASE_URI)

Session = sessionmaker(bind=engine)


class Database:

    def __init__(self):
        self.connection = None
        self.cursor = None
        self.session = None
        self.engine = engine

    def connect(self):
        try:
            self.session = Session()
            self.connection = engine.connect()
            print("Connected")
        except (Exception, psycopg2.Error) as error:
            print("Failed connection to PostgreSQL", error)

    def close(self):
        if self.session:
            self.session.close()
            print("\nConnection closed")

    def add_product(self, prod):
        try:
            prod.id = Sequence('products_id_seq').next_value()
            self.session.add(prod)
            self.session.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed adding product to the database:", error)
            self.session.rollback()

    def add_store(self, store):
        try:
            store.id = Sequence('stores_id_seq').next_value()
            self.session.add(store)
            self.session.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed adding store to the database:", error)
            self.session.rollback()

    def add_sale(self, sale):
        try:
            sale.id = Sequence('sales_id_seq').next_value()
            self.session.add(sale)
            self.session.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed adding sale to the database:", error)
            self.session.rollback()

    def add_price(self, price):
        try:
            price.id = Sequence('prices_id_seq').next_value()
            self.session.add(price)
            self.session.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed adding price to the database:", error)
            self.session.rollback()

    def generate_sales(self, n):
        try:
            statement1 = text("""with expanded as (select random(), seq, products.id from generate_series(1, %s)
                            seq, products),
                            shuffled as ( SELECT e.* FROM expanded e INNER JOIN (
                            SELECT ei.seq, MIN(ei.random) FROM expanded ei GROUP BY ei.seq)
                             em ON (e.seq = em.seq AND e.random = em.min)
                            ORDER BY e.seq)
                            select id from shuffled""" % int(n))
            res = self.connection.execute(statement1)
            prod_ids = res.fetchall()
            statement2 = text("""with expanded as (select random(), seq, stores.id from generate_series(1, %s)
                                        seq, stores),
                                        shuffled as ( SELECT e.* FROM expanded e INNER JOIN (
                                        SELECT ei.seq, MIN(ei.random) FROM expanded ei GROUP BY ei.seq)
                                         em ON (e.seq = em.seq AND e.random = em.min)
                                        ORDER BY e.seq)
                                        select id from shuffled""" % int(n))
            res = self.connection.execute(statement2)
            stor_ids = res.fetchall()
            statement3 = text("""select (timestamp '2020-01-01 00:00:00' +
                            random() * (timestamp '2021-01-01 00:00:00' - timestamp '2020-01-01 00:00:00'))::date
                             as date 
                            from generate_series(1, %s)""" % n)
            res = self.connection.execute(statement3)
            dates = res.fetchall()
            statement4 = text("""select round(random()*1000 + 1)::int from generate_series(1, %s)""" % n)
            res = self.connection.execute(statement4)
            amounts = res.fetchall()
            i = 0
            while i < n:
                sale = Sale(0, stor_ids[i].id, prod_ids[i].id, amounts[i].round, dates[i].date)
                self.add_sale(sale)
                i += 1
        except (Exception, psycopg2.Error) as error:
            print("Failed adding price to the database:", error)
            self.session.rollback()

    def generate_prices(self, n):
        try:
            statement1 = text("""with expanded as (select random(), seq, products.id from generate_series(1, %s)
                seq, products),
                shuffled as ( SELECT e.* FROM expanded e INNER JOIN (
                SELECT ei.seq, MIN(ei.random) FROM expanded ei GROUP BY ei.seq)
                 em ON (e.seq = em.seq AND e.random = em.min)
                ORDER BY e.seq)
                select id from shuffled""" % int(n))
            res = self.connection.execute(statement1)
            ids = res.fetchall()
            statement2 = text("""select (timestamp '2020-01-01 00:00:00' +
                random() * (timestamp '2021-01-01 00:00:00' - timestamp '2020-01-01 00:00:00'))::date as date 
                from generate_series(1, %s)""" % n)
            res = self.connection.execute(statement2)
            dates = res.fetchall()
            statement3 = text("""select round((2*random()* 100 + random()* 100 - random()* 10)::numeric, 2) as price
                from generate_series(1, %s)""" % n)
            res = self.connection.execute(statement3)
            prices = res.fetchall()
            i = 0
            while i < n:
                price = Price(0, ids[i].id, float(prices[i].price), str(dates[i].date))
                self.add_price(price)
                i += 1
        except (Exception, psycopg2.Error) as error:
            print("Failed generating prices:", error)
            self.session.rollback()





