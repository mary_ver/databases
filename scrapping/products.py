from bs4 import BeautifulSoup
import requests
from models import Product


headers = {
        'authority': 'citymarket.zakaz.ua',
        'cache-control': 'max-age=0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'
                      ' (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
        'sec-fetch-dest': 'document',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;'
                  'q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'accept-language': 'en-US,en;q=0.9',
    }
session = requests.Session()


def scrap_prod(url, num_pages, products):
    i = 1
    while i <= num_pages:
        response = session.get(url,
                               params={'page': '%s' % i}, headers=headers)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            category = soup.find('h1', attrs={'class': 'jsx-2761681322 SecondLevelCategory__title'})
            for a in soup.find_all('div', attrs={'class': 'jsx-899526249 products-box__list-item'}):
                name = a.find('span', attrs={'class': 'jsx-3203167493 product-tile__title'})
                products.append(Product(0, name.text, category.text))
        else:
            print("Bad result")
        i += 1
