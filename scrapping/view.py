class View:
    @staticmethod
    def into():
        print("-----Generation of data-------")
        print("Here you can generate data for your needs")

    @staticmethod
    def options():
        print("OPTIONS:\n 1) Scrap products\n 2) Scrap addresses\n 3) Generate random values")
        print("To exit print 0")

    @staticmethod
    def get_option():
        opt = input("Enter the option:")
        try:
            int(opt)
        except Exception:
            print("Incorrect input of option")
            return None
        return int(opt)

    @staticmethod
    def incorrect_opt():
        print("The option you've entered is out of range. Try again")

    @staticmethod
    def outro():
        print("Thanks for using!")

    @staticmethod
    def chose_table_gen():
        print("Chose the table for generation:\n 1)Sales\n 2)Prices")
        opt = input("Option:")
        try:
            int(opt)
        except Exception:
            print("Incorrect input of option")
            return None
        return int(opt)

    @staticmethod
    def choose_number():
        opt = input("Choose number of records:")
        try:
            int(opt)
        except Exception:
            print("Incorrect input of number")
            return None
        return int(opt)
