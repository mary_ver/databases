from bs4 import BeautifulSoup
import requests


add_headers = {
        'authority': 'www.bestrandoms.com',
        'cache-control': 'max-age=0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'
                      ' (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
        'sec-fetch-dest': 'document',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;'
                  'q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'cross-site',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'accept-language': 'en-US,en;q=0.9',

    }
session = requests.Session()


def scrap_add():
    addresses = []
    i = 1
    while i <= 5:
        response = session.get("https://www.bestrandoms.com/random-address-in-ua", params={'quantity': '20'},
                               headers=add_headers)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            ad_list = soup.find_all('li', attrs={'class': 'col-sm-6'})
            for a in ad_list:
                street = a.find('p')
                street_crop = str(street.text).strip('Street:  ').strip()
                city = street.findNext('p')
                city_crop = str(city.text).strip('City:').strip()
                address = street_crop + ', ' + city_crop
                addresses.append(address)
        else:
            print("Bad result")
        i += 1
    return addresses
