from models import Bank, Account, Card, Client, Transaction, CardAccount


class Controller:
    def __init__(self, database, view):
        self.database = database
        self.view = view
        self.option = None
        self.model = None

    def start(self):
        self.view.intro()
        self.view.show_options()
        self.database.connect()
        self.option = self.view.get_option()

    def end(self):
        self.database.close()
        self.view.outro()

    def get(self):
        if self.option == '6':
            c_id = input("Enter card id:")
            a_id = input("Enter account id:")
            self.model = self.database.get_card_account(a_id, c_id)
        else:
            e_id = input("Enter the id:")
            if self.option == '1':
                self.model = self.database.get_acc(e_id)
            elif self.option == '2':
                self.model = self.database.get_bank(e_id)
            elif self.option == '3':
                self.model = self.database.get_card(e_id)
            elif self.option == '4':
                self.model = self.database.get_client(e_id)
            elif self.option == '5':
                self.model = self.database.get_transaction(e_id)
            else:
                self.view.incorrect_opt()

    def add(self):
        if self.option == '1':
            new_ac = self.view.get_model("Account", 0)
            if new_ac is None:
                return
            new_ac.id = self.database.add_account(new_ac)
            self.view.return_id(new_ac.id, new_ac)
        elif self.option == '2':
            ans = self.view.generate()
            if ans == 'y' or ans == 'Y':
                number = self.view.get_number_of_rec()
                if number is None:
                    return
                self.database.generate_b(number)
                return
            new_ban = self.view.get_model("Bank", 0)
            if new_ban is None:
                return
            new_ban.id = self.database.add_bank(new_ban)
            self.view.return_id(new_ban.id, new_ban)
        elif self.option == '3':
            ans = self.view.generate()
            if ans == 'y' or ans == 'Y':
                number = self.view.get_number_of_rec()
                if number is None:
                    return
                self.database.generate_c(number)
                return
            new_c = self.view.get_model("Card", 0)
            if new_c is None:
                return
            new_c.id = self.database.add_card(new_c)
            self.view.return_id(new_c.id, new_c)
        elif self.option == '4':
            new_cl = self.view.get_model("Client", 0)
            if new_cl is None:
                return
            new_cl.ITN = self.database.add_client(new_cl)
            self.view.return_id(new_cl.ITN, new_cl)
        elif self.option == '5':
            new_t = self.view.get_model("Transaction", 0)
            if new_t is None:
                return
            new_t.id = self.database.add_transaction(new_t)
            self.view.return_id(new_t.id, new_t)
        elif self.option == '6':
            c_a = self.view.get_model("Card/Account", 0)
            if c_a is None:
                return
            self.database.add_card_account(c_a)
        else:
            self.view.incorrect_opt()

    def upd(self):
        if self.option == '1':
            _id = self.view.get_id()
            if _id is None:
                return
            acc = self.database.get_acc(_id)
            if not acc:
                return
            self.view.upd_info()
            new_ac = self.view.upd_model(acc)
            if new_ac is None:
                return
            self.database.update_account(new_ac)
        elif self.option == '2':
            _id = self.view.get_id()
            if _id is None:
                return
            bank = self.database.get_bank(_id)
            if not bank:
                return
            self.view.upd_info()
            new_ban = self.view.upd_model(bank)
            self.database.update_bank(new_ban)
        elif self.option == '3':
            _id = self.view.get_id()
            if _id is None:
                return
            card = self.database.get_card(_id)
            if not card:
                return
            self.view.upd_info()
            new_c = self.view.upd_model(card)
            if new_c is None:
                return
            self.database.update_card(new_c)
        elif self.option == '4':
            _id = self.view.get_id()
            if _id is None:
                return
            client = self.database.get_client(_id)
            if not client:
                return
            self.view.upd_info()
            new_cl = self.view.upd_model(client)
            if new_cl is None:
                return
            self.database.update_client(new_cl)
        elif self.option == '5':
            _id = self.view.get_id()
            if _id is None:
                return
            trans = self.database.get_transaction(_id)
            if not trans:
                return
            self.view.upd_info()
            new_t = self.view.upd_model(trans)
            if new_t is None:
                return
            self.database.update_transaction(new_t)
        elif self.option == '6':
            a_id = input("Enter account id to update relation:")
            try:
                a_id = int(a_id)
            except Exception:
                self.view.incorrect_input("account id")
                return
            c_id = input("Enter card id to update relation:")
            try:
                c_id = int(c_id)
            except Exception:
                self.view.incorrect_input("card id")
                return
            c_a = self.database.get_card_account(a_id, c_id)
            if not c_a:
                return
            self.view.upd_info()
            new_c_a = self.view.upd_model(c_a)
            if new_c_a is None:
                return
            self.database.update_card_account(new_c_a)
        else:
            self.view.incorrect_opt()

    def delete(self):
        if self.option == '6':
            c_id = input("Enter card id:")
            a_id = input("Enter account id:")
            self.database.del_card_account(CardAccount(a_id, c_id))
        else:
            e_id = input("Enter the id:")
            if self.option == '1':
                acc = self.database.get_acc(e_id)
                if acc:
                    self.database.del_account(acc)
            elif self.option == '2':
                b = self.database.get_bank(e_id)
                if b:
                    self.database.del_bank(b)
            elif self.option == '3':
                card = self.database.get_card(e_id)
                if card:
                    self.database.del_card(card)
            elif self.option == '4':
                client = self.database.get_client(e_id)
                if client:
                    self.database.del_client(client)
            elif self.option == '5':
                tr = self.database.get_transaction(e_id)
                if tr:
                    self.database.del_transaction(e_id)
            else:
                self.view.incorrect_opt()

    def query(self):
        if self.option == '1':
            diction = self.view.query1_get_args()
            owner = diction.get('owner')
            type = diction.get('type')
            r_start = diction.get('r_start')
            r_end = diction.get('r_end')
            clients = self.database.query1(owner, type, int(r_start), int(r_end))
            if clients is None:
                return
            for row in clients:
                date = str(row[2]).replace('-', '.')
                c = Client(row[0], row[1], date, row[3], row[4])
                self.view.print_model(c)
        elif self.option == '2':
            place = self.view.query2_get_args()
            banks = self.database.query2(place)
            if banks is None:
                return
            for row in banks:
                bank = Bank(row[0], row[1])
                self.view.print_model(bank)
        elif self.option == '3':
            diction = self.view.query3_get_args()
            name = diction.get('name')
            bank_id = diction.get('bank_id')
            d_start = diction.get('d_start')
            d_end = diction.get('d_end')
            accounts = self.database.query3(name, bank_id, d_start, d_end)
            if accounts is None:
                return
            for row in accounts:
                self.view.print_model(row)
        else:
            self.view.incorrect_opt()





