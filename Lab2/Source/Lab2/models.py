class Bank:
    def __init__(self, b_id, owner):
        self.id = int(b_id)
        self.owner = str(owner)


class Account:
    def __init__(self, a_id, cl_id, b_id, a_type, a_sum, date):
        self.id = int(a_id)
        self.client_id = int(cl_id)
        self.bank_id = int(b_id)
        self.type = str(a_type)
        self.sum = float(a_sum)
        self.start_date = str(date)


class Card:
    def __init__(self, c_id, b_id, exp_date):
        self.id = int(c_id)
        self.b_id = int(b_id)
        self.exp_date = str(exp_date)


class Client:
    def __init__(self, itn, fullname, date_birth, phone, email):
        self.ITN = int(itn)
        self.fullname = str(fullname)
        self.date_birth = str(date_birth)
        self.phone = int(phone)
        self.email = str(email)


class Transaction:
    def __init__(self, t_id, a_id, t_sum, date, place):
        self.id = int(t_id)
        self.a_id = int(a_id)
        self.sum = float(t_sum)
        self.date = str(date)
        self.place = str(place)


class CardAccount:
    def __init__(self, c_id, a_id):
        self.c_id = int(c_id)
        self.a_id = int(a_id)
