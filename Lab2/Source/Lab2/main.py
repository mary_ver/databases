from database import Database
from controller import Controller
from view import View


def main():
    db = Database()
    view = View()
    controller = Controller(db, view)
    controller.start()
    option = controller.option
    while controller.option != '0':
        if option == '1':
            controller.view.opt_get()
            controller.option = controller.view.get_option()
            controller.get()
            if controller.model:
                controller.view.print_model(controller.model)
                del controller.model
        elif option == '2':
            controller.view.opt_add()
            controller.option = controller.view.get_option()
            controller.add()
        elif option == '3':
            controller.view.opt_upd()
            controller.option = controller.view.get_option()
            controller.upd()
        elif option == '4':
            controller.view.opt_del()
            controller.option = controller.view.get_option()
            controller.delete()
        elif option == '5':
            controller.option = controller.view.opt_query()
            controller.query()
        else:
            controller.view.incorrect_opt()
        controller.view.show_options()
        controller.option = controller.view.get_option()
        option = controller.option
    controller.end()


if __name__ == '__main__':
    main()
