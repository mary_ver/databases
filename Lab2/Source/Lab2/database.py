import psycopg2
import math
import time
from models import Account, Card, Client, Bank, Transaction, CardAccount
import psycopg2.extensions
from psycopg2.extras import LoggingConnection, LoggingCursor
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class MyLoggingCursor(LoggingCursor):
    def execute(self, query, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).execute(query, vars)

    def callproc(self, procname, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).callproc(procname, vars)


class MyLoggingConnection(LoggingConnection):
    def filter(self, msg, curs):
        return "%s ms" % int((time.time() - curs.timestamp) * 1000)

    def cursor(self, *args, **kwargs):
        kwargs.setdefault('cursor_factory', MyLoggingCursor)
        return LoggingConnection.cursor(self, *args, **kwargs)


class Database:

    def __init__(self):
        self.connection = None
        self.cursor = None

# WORKS
    def connect(self):
        try:
            self.connection = psycopg2.connect(user="postgres",
                                               password="qwerty",
                                               host="127.0.0.1",
                                               port="5432",
                                               database="lab1_bank")
            self.cursor = self.connection.cursor()
        except (Exception, psycopg2.Error) as error:
            print("Failed connection to PostgreSQL", error)
            self.connection.rollback()

# WORKS
    def close(self):
        if self.connection:
            self.cursor.close()
            self.connection.close()
            print("\nConnection closed")

    def get_bank(self, bank_id):
        try:
            self.cursor.execute(
                "select * from public.\"Bank\" where public.\"Bank\".\"EDRPOU\" = %s" % (
                    bank_id
                )
            )
            self.connection.commit()
            banks = self.cursor.fetchall()
            row = banks[0]
            b = Bank(row[0], row[1])
            return b
        except (Exception, psycopg2.Error) as error:
            print("Failed getting bank:", error)
            self.connection.rollback()

# WORKS
    def add_bank(self, bank):
        try:
            self.cursor.execute(
                "insert into public.\"Bank\" (\"Owner\") values (\'%s\')" % (
                    bank.owner
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "select last_value::int from b_ser"
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            id1 = str(res[0]).replace('(', '')
            id2 = int(id1.replace(',)', ''))
            return id2
        except (Exception, psycopg2.Error) as error:
            print("Failed adding bank to the database", error)
            self.connection.rollback()

# WORKS
    def update_bank(self, bank):
        try:
            self.cursor.execute(
                "update public.\"Bank\" set \"Owner\" = \'%s\' where \"Bank\".\"EDRPOU\" = %s"
                % (
                    bank.owner, bank.id
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such bank in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the bank:", error)
            self.connection.rollback()

    def del_bank(self, bank):
        try:
            self.cursor.execute(
                "select * from public.\"Account\" as ac where ac.\"Bank_Id\" = %s " % (
                    bank.id
                )
            )
            self.connection.commit()
            accs = self.cursor.fetchall()
            for row in accs:
                date = str(row[5]).replace('-', '.')
                self.cursor.execute(
                    "select (select t.\"Sum\" from public.\"Account\" as t where t.\"ID\" = %s)::money::numeric::float8"
                    % row[0]
                )
                self.connection.commit()
                res = self.cursor.fetchall()
                sum1 = str(res[0]).replace('(', '')
                sum2 = sum1.replace(',)', '')
                ac = Account(row[0], row[1], row[2], row[3], sum2, date)
                self.del_account(ac)
            self.cursor.execute(
                "delete from public.\"Card\" as c where c.\"Bank_Id\" = %s" % (
                    bank.id
                )
            )
            self.cursor.execute(
                "delete from public.\"Bank\" where public.\"Bank\".\"EDRPOU\" = %s" % (
                    bank.id
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting bank in the database:", error)
            self.connection.rollback()

    def get_acc(self, account_id):
        try:
            self.cursor.execute(
                "select * from public.\"Account\" where public.\"Account\".\"ID\" = %s" % (
                    account_id
                )
            )
            self.connection.commit()
            accs = self.cursor.fetchall()
            row = accs[0]
            date = str(row[5]).replace('-', '.')
            self.cursor.execute(
                "select (select t.\"Sum\" from public.\"Account\" as t where t.\"ID\" = %s)::money::numeric::float8"
                % account_id
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            sum1 = str(res[0]).replace('(', '')
            sum2 = sum1.replace(',)', '')
            ac = Account(row[0], row[1], row[2], row[3], sum2, date)
            return ac
        except (Exception, psycopg2.Error) as error:
            print("Failed getting account:", error)
            self.connection.rollback()

# WORKS
    def add_account(self, account):
        try:
            self.cursor.execute(
                "insert into public.\"Account\" "
                "(\"Client_Id\", \"Bank_Id\", \"Type\", \"Sum\", \"Start_date\")"
                " values (%s, %s, \'%s\', %s, \'%s\')" % (
                    account.client_id, account.bank_id, account.type, account.sum, account.start_date
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "select last_value::int from a_ser"
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            id1 = str(res[0]).replace('(', '')
            id2 = int(id1.replace(',)', ''))
            return id2
        except (Exception, psycopg2.Error) as error:
            print("Failed adding account to the database:", error)
            self.connection.rollback()

# WORKS
    def update_account(self, account):
        try:
            self.cursor.execute(
                "update public.\"Account\" set \"Type\" = \'%s\', \"Client_Id\" = %s,"
                " \"Bank_Id\" = %s, \"Sum\" = %s, \"Start_date\" = \'%s\' where \"Account\".\"ID\" = %s"
                % (
                    account.type, account.client_id, account.bank_id, account.sum, account.start_date, account.id
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such account in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the account:", error)
            self.connection.rollback()

# WORKS seems to...
    def del_account(self, account):
        try:
            self.cursor.execute(
                "delete from public.\"Card/Account\" where public.\"Card/Account\".\"Account_Id\" = %s" % (
                    account.id
                )
            )
            self.cursor.execute(
                "delete from public.\"Transaction\" where public.\"Transaction\".\"Account_Id\" = %s" % (
                    account.id
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "delete from public.\"Account\" where public.\"Account\".\"ID\" = %s" % (
                    account.id
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting account in the database:", error)
            self.connection.rollback()

    def get_client(self, client_id):
        try:
            self.cursor.execute(
                "select * from public.\"Client\" where public.\"Client\".\"ITN\" = %s" % (
                    client_id
                )
            )
            self.connection.commit()
            clis = self.cursor.fetchall()
            row = clis[0]
            date = str(row[2]).replace('-', '.')
            c = Client(row[0], row[1], date, row[3], row[4])
            return c
        except (Exception, psycopg2.Error) as error:
            print("Failed getting client:", error)
            self.connection.rollback()

    def add_client(self, client):
        try:
            self.cursor.execute(
                "insert into public.\"Client\" "
                "(\"Fullname\", \"Date_of_birth\", \"Phone\", \"Email\") values (\'%s\', \'%s\', %s, \'%s\')" % (
                    client.fullname, client.date_birth, client.phone, client.email
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "select last_value::int from cl_ser"
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            id1 = str(res[0]).replace('(', '')
            id2 = int(id1.replace(',)', ''))
            return id2
        except (Exception, psycopg2.Error) as error:
            print("Failed adding client to the database:", error)
            self.connection.rollback()

    def update_client(self, client):
        try:
            self.cursor.execute(
                "update public.\"Client\" set \"Fullname\" = \'%s\', \"Date_of_birth\" = \'%s\',"
                " \"Phone\" = %s, \"Email\" = \'%s\' where \"Client\".\"ITN\" = %s"
                % (
                    client.fullname, client.date_birth, client.phone, client.email, client.ITN
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such client in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the client:", error)
            self.connection.rollback()

    def del_client(self, client):
        try:
            self.cursor.execute(
                "select * from public.\"Account\" where public.\"Account\".\"Client_Id\" = %s" % (
                    client.ITN
                )
            )
            self.connection.commit()
            accs = self.cursor.fetchall()
            for row in accs:
                self.cursor.execute(
                    "select (select t.\"Sum\" from public.\"Account\" as t where t.\"ID\" = %s)::money::numeric::float8"
                    % row[0]
                )
                self.connection.commit()
                res = self.cursor.fetchall()
                sum1 = str(res[0]).replace('(', '')
                sum2 = sum1.replace(',)', '')
                date = str(row[5]).replace('-', '.')
                ac = Account(row[0], row[1], row[2], row[3], sum2, date)
                self.del_account(ac)
            self.cursor.execute(
                "delete from public.\"Client\" where public.\"Client\".\"ITN\" = %s" % (
                    client.ITN
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting the client in the database:", error)
            self.connection.rollback()

    def get_card(self, card_id):
        try:
            self.cursor.execute(
                "select * from public.\"Card\" where public.\"Card\".\"ID\" = %s" % (
                    card_id
                )
            )
            self.connection.commit()
            cards = self.cursor.fetchall()
            row = cards[0]
            date = str(row[2]).replace('-', '.')
            c = Card(row[0], row[1], date)
            return c
        except (Exception, psycopg2.Error) as error:
            print("Failed getting card", error)
            self.connection.rollback()

    def add_card(self, card):
        try:
            self.cursor.execute(
                "insert into public.\"Card\" (\"Bank_Id\", \"Exp_date\") values (%s, \'%s\')" % (
                    card.b_id, card.exp_date
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "select last_value::int from c_ser"
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            id1 = str(res[0]).replace('(', '')
            id2 = int(id1.replace(',)', ''))
            return id2
        except (Exception, psycopg2.Error) as error:
            print("Failed adding card to the database:", error)
            self.connection.rollback()

    def update_card(self, card):
        try:
            self.cursor.execute(
                "update public.\"Card\" set \"Bank_Id\" = %s, \"Exp_date\" = \'%s\' "
                "where \"Card\".\"ID\" = %s"
                % (
                    card.b_id, card.exp_date, card.id
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such card in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the card:", error)
            self.connection.rollback()

    def del_card(self, client):
        try:
            self.cursor.execute(
                "delete from public.\"Card/Account\" where public.\"Card/Account\".\"Card_Id\" = %s" % (
                    client.id
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "delete from public.\"Card\" where public.\"Card\".\"ID\" = %s" % (
                    client.id
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting card in the database:", error)
            self.connection.rollback()

    def get_transaction(self, trans_id):
        try:
            self.cursor.execute(
                "select * from public.\"Transaction\" where public.\"Transaction\".\"ID\" = %s" % (
                    trans_id
                )
            )
            self.connection.commit()
            trs = self.cursor.fetchall()
            row = trs[0]
            date = str(row[3]).replace('-', '.')
            self.cursor.execute(
                "select (select t.\"Sum\" from public.\"Transaction\" as t where t.\"ID\" = %s)::money::numeric::float8"
                % trans_id
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            sum1 = str(res[0]).replace('(', '')
            sum2 = sum1.replace(',)', '')
            t = Transaction(row[0], row[1], sum2, date, row[4])
            return t
        except (Exception, psycopg2.Error) as error:
            print("Failed getting transaction:", error)
            self.connection.rollback()

# WORKS
    def add_transaction(self, transaction):
        try:
            self.cursor.execute(
                "insert into public.\"Transaction\" "
                "(\"Account_Id\", \"Sum\", \"Date\", \"Place\") values (%s, %s, \'%s\', \'%s\')" % (
                    transaction.a_id, transaction.sum, transaction.date, transaction.place
                )
            )
            self.connection.commit()
            self.cursor.execute(
                "select last_value::int from t_ser"
            )
            self.connection.commit()
            res = self.cursor.fetchall()
            id1 = str(res[0]).replace('(', '')
            id2 = int(id1.replace(',)', ''))
            return id2
        except (Exception, psycopg2.Error) as error:
            print("Failed adding transaction to the database:", error)
            self.connection.rollback()

# WORKS
    def update_transaction(self, transaction):
        try:
            self.cursor.execute(
                "update public.\"Transaction\" set \"Account_Id\" = %s, \"Sum\" = %s, "
                "\"Date\" = \'%s\', \"Place\" = \'%s\' "
                "where \"Transaction\".\"ID\" = %s"
                % (
                    transaction.a_id, transaction.sum, transaction.date, transaction.place, transaction.id
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such transaction in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the transaction:", error)
            self.connection.rollback()

# WORKS
    def del_transaction(self, transaction):
        try:
            self.cursor.execute(
                "delete from public.\"Transaction\" where public.\"Transaction.ID\" = %s" % (
                    transaction.id
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting transaction in the database:", error)
            self.connection.rollback()

    def get_card_account(self, a_id, c_id):
        try:
            self.cursor.execute(
                "select * from public.\"Card/Account\" as ca where ca.\"Account_Id\" = %s and "
                "ca.\"Card_Id\" = %s" % (
                    a_id, c_id
                )
            )
            self.connection.commit()
            trs = self.cursor.fetchall()
            row = trs[0]
            ca = CardAccount(row[0], row[1])
            return ca
        except (Exception, psycopg2.Error) as error:
            print("Failed getting transaction:", error)
            self.connection.rollback()

# WORKS
    def add_card_account(self, card_account):
        try:
            self.cursor.execute(
                "insert into public.\"Card/Account\" values (%s, %s)" % (
                    card_account.c_id, card_account.a_id
                )
            )
            self.connection.commit()
            print("Added successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed adding card-account to the database:", error)
            self.connection.rollback()

    def update_card_account(self, card_account):
        try:
            self.cursor.execute(
                "update public.\"Card/Account\" set \"Card_Id\" = %s "
                "where \"Card/Account\".\"Account_Id\" = %s"
                % (
                    card_account.c_id, card_account.a_id
                )
            )
            self.connection.commit()
            affected = self.cursor.rowcount
            if affected == 0:
                raise Exception('No such client in database')
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the card-account:", error)
            self.connection.rollback()

# WORKS
    def del_card_account(self, card_account):
        try:
            self.cursor.execute(
                "delete from public.\"Card/Account\" where public.\"Card/Account\".\"Card_Id\" = %s "
                "and public.\"Card/Account\".\"Account_Id\" = %s" % (
                    card_account.c_id, card_account.a_id
                )
            )
            self.connection.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting card-account in the database:", error)
            self.connection.rollback()

    def generate_b(self, number):
        try:
            self.cursor.execute(
                "insert into \"Bank\" (\"Owner\") select chr(trunc(65+random()*25)::int) || "
                "chr(trunc(65+random()*25)::int)"
                " || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int) as owner "
                "from generate_series(1, %s)" % number
            )
            self.connection.commit()
            print("Generated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed generating:", error)
            self.connection.rollback()

    def generate_c(self, number):
        try:
            self.cursor.execute(
                "WITH expanded AS ( SELECT RANDOM(), seq, b.\"EDRPOU\" AS bank_id "
                "FROM GENERATE_SERIES(1, %s) seq, \"Bank\" b), "
                "shuffled AS ( SELECT e.* FROM expanded e INNER JOIN ("
                "SELECT ei.seq, MIN(ei.random) FROM expanded ei GROUP BY ei.seq)"
                " em ON (e.seq = em.seq AND e.random = em.min)"
                "ORDER BY e.seq),"
                "dates as (select (timestamp '2000-01-10 00:00:00' +"
                "random() * (timestamp '2021-01-01 00:00:00' - timestamp '2000-01-01 00:00:00'))::date as n_date "
                "from generate_series(1, %s)) insert into \"Card\" (\"Bank_Id\", \"Exp_date\") "
                "select s.bank_id, d.n_date from shuffled as s, dates as d group by random(), s.bank_id, d.n_date"
                " limit %s" % (
                    int(math.sqrt(int(number))) + 1,  int(math.sqrt(int(number))) + 1, int(number)
                )
            )
            self.connection.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed generating:", error)
            self.connection.rollback()

    def query1(self, owner, type, start, end):
        self.connection.close()
        self.cursor.close()
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()

        try:
            self.cursor.execute(
                "select * from \"Client\" where \"ITN\" = "
                "(select \"Client_Id\" from \"Account\" "
                "where \"Bank_Id\" = (select \"EDRPOU\" "
                "as b_id from \"Bank\" where \"Owner\" like %s) "
                "and \"Type\" like %s and \"Sum\"::money::numeric::float8 "
                "between %s and %s)" % (str("'%"+owner+"%'"), str("'"+type+"'"), start, end)
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No client to match your query")
            clients = self.cursor.fetchall()
            self.connection.close()
            self.connect()
            return clients
        except (Exception, psycopg2.Error) as error:
            print("Error: ", error)
            self.connection.close()
            self.connect()
            self.connection.rollback()
            return None

    def query2(self, place):
        self.connection.close()
        self.cursor.close()
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute(
                "with count_times as (select \"Account_Id\", count(\"Account_Id\") as n from \"Transaction\" "
                "where \"Place\" like %s group by \"Account_Id\"),"
                " sel_max as (select \"Account_Id\", n "
                "from count_times "
                "where n = (select max(n) from count_times)"
                " group by \"Account_Id\", n) "
                "select * from \"Bank\" as c "
                "inner join "
                "(select \"Bank_Id\" from \"Account\" as a "
                "inner join "
                "sel_max as b "
                "on a.\"ID\"=b.\"Account_Id\") as d "
                "on c.\"EDRPOU\"=d.\"Bank_Id\"" % str("'%"+place+"%'")
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No bank to match your query")
            banks = self.cursor.fetchall()
            self.connection.close()
            self.connect()
            return banks
        except (Exception, psycopg2.Error) as error:
            print ("Error: ", error)
            self.connection.close()
            self.connect()
            self.connection.rollback()
            return None

    def query3(self, name, bank_id, d_start, d_end):
        self.connection.close()
        self.cursor.close()
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute(
                "with accs as (select * from \"Account\" as a "
                "where \"Start_date\"::date between timestamp '%s' and timestamp '%s' "
                "and \"Bank_Id\" = %s)"
                "select * from accs as a "
                "inner join "
                "(select \"ITN\" from \"Client\" where \"Fullname\" like %s) as b "
                "on a.\"Client_Id\" = b.\"ITN\"" % (d_start, d_end, int(bank_id), str("'%"+name+"%'"))
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No account to match your query")
            accs = self.cursor.fetchall()
            accounts = []
            for row in accs:
                date = str(row[5]).replace('-', '.')
                self.cursor.execute(
                    "select (select t.\"Sum\" from public.\"Account\" as t where t.\"ID\" = %s)::money::numeric::float8"
                    % row[0]
                )
                self.connection.commit()
                res = self.cursor.fetchall()
                sum1 = str(res[0]).replace('(', '')
                sum2 = sum1.replace(',)', '')
                ac = Account(row[0], row[1], row[2], row[3], sum2, date)
                accounts.append(ac)
            self.connection.close()
            self.connect()
            return accounts
        except (Exception, psycopg2.Error) as error:
            print("Error: ", error)
            self.connection.close()
            self.connect()
            self.connection.rollback()
            return None

