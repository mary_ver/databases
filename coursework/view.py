from pandas import to_datetime


class View:
    @staticmethod
    def intro():
        print("-----Analysis-------")
        print("The tool for analysing sales and prices for data of chain stores")

    @staticmethod
    def options():
        print("OPTIONS:\n"
              "1) Analyze the number of sales of the certain product\n"
              "2) Analyze the price trend for the product\n"
              "3) Forecast the price of the product for the next n months\n")
        print("To exit print 0")

    @staticmethod
    def get_option():
        opt = input("Enter the option:")
        try:
            int(opt)
        except Exception:
            print("Incorrect input of option")
            return None
        return int(opt)

    @staticmethod
    def get_product_id():
        in_id = input("Enter the product id: ")
        try:
            int(in_id)
        except Exception:
            print("Incorrect input of id")
            return None
        return int(in_id)

    @staticmethod
    def incorrect_opt():
        print("The option you've entered is out of range. Try again")

    @staticmethod
    def outro():
        print("Thanks for using!")

    @staticmethod
    def choose_nmonths():
        opt = input("Choose number of months:")
        try:
            int(opt)
        except Exception:
            print("Incorrect input of number")
            return None
        return int(opt)

    @staticmethod
    def choose_dates():
        try:
            in_start_d = input("Enter the start date of period: ")
            start_d = str(to_datetime(in_start_d, errors='raise'))
            in_end_d = input("Enter the end date of period: ")
            end_d = str(to_datetime(in_end_d, errors='raise'))
            dates = {'start': start_d, 'end': end_d}
            return dates
        except Exception as err:
            print("Incorrect input: %s", err)
            return None

    @staticmethod
    def no_product():
        print("No such product")
