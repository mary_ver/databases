import psycopg2
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker

MASTER_DATABASE_URI = 'postgres+psycopg2://postgres:qwerty@192.168.56.101:5432/store'
SLAVE_DATABASE_URI = 'postgres+psycopg2://postgres:qwerty@192.168.56.102:5432/store'
engine_master = create_engine(MASTER_DATABASE_URI)
engine_slave = create_engine(SLAVE_DATABASE_URI)

SessionM = sessionmaker(bind=engine_master)
SessionS = sessionmaker(bind=engine_slave)


class Database:

    def __init__(self):
        self.connection = None
        self.cursor = None
        self.session = None
        self.engine = None

    def try_connection(self):
        try:
            self.engine = engine_master
            self.session = SessionM()
            self.connection = engine_master.connect()
        except Exception as error:
            self.engine = engine_slave
            self.session = SessionS()
            self.connection = engine_slave.connect()

    def connect(self):
        try:
            self.try_connection()
            print("Connected")
            return True
        except (Exception, psycopg2.Error) as error:
            print("Failed connection to PostgreSQL", error)
            return False

    def close(self):
        if self.session:
            self.session.close()

    def get_product(self, prod_id):
        try:
            statement = text("""select name from products where id = %s""" % prod_id)
            self.close()
            self.try_connection()
            res = self.connection.execute(statement)
            row = res.fetchall()
            name = row[0].name
            return name
        except (Exception, psycopg2.Error) as error:
            print("Error occurred: ", error)
            return None
