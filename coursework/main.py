from controller import Controller


def main():
    controller = Controller()
    try:
        controller.start()
    except Exception as err:
        print(err)
        return

    controller.view.options()
    controller.option = controller.view.get_option()
    while controller.option != 0:
        if controller.option == 1:
            controller.product_id = controller.view.get_product_id()
            if controller.product_id is None:
                continue
            else:
                prod_name = controller.database.get_product(controller.product_id)
                if prod_name is None:
                    controller.view.no_product()
                    continue
                else:
                    controller.prod_name = prod_name
                    dates = controller.view.choose_dates()
                    if dates is not None:
                        controller.sales(dates['start'], dates['end'])
                    else:
                        continue
        elif controller.option == 2:
            controller.product_id = controller.view.get_product_id()
            if controller.product_id is None:
                continue
            else:
                prod_name = controller.database.get_product(controller.product_id)
                if prod_name is None:
                    controller.view.no_product()
                    continue
                else:
                    controller.prod_name = prod_name
                    controller.trend()
        elif controller.option == 3:
            controller.product_id = controller.view.get_product_id()
            if controller.product_id is None:
                continue
            else:
                prod_name = controller.database.get_product(controller.product_id)
                if prod_name is None:
                    controller.view.no_product()
                    continue
                else:
                    controller.prod_name = prod_name
                    months = controller.view.choose_nmonths()
                    if months is not None:
                        controller.forecast(months)
                    else:
                        continue
        else:
            controller.view.incorrect_opt()
        controller.view.options()
        controller.option = controller.view.get_option()

    controller.stop()


if __name__ == '__main__':
    main()
