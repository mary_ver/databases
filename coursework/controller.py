from analysis import *
from database import Database
from view import View


class Controller:
    option = -1
    product_id = -1
    prod_name = ''
    database = Database()
    view = View()

    def start(self):
        if self.database.connect():
            self.view.intro()
        else:
            raise Exception("Unable to connect")

    def stop(self):
        self.database.close()
        self.view.outro()

    def trend(self):
        price_trend(self.database, self.product_id, self.prod_name)

    def sales(self, start_date, end_date):
        sales_diagram(self.database, self.product_id, self.prod_name, start_date, end_date)

    def forecast(self, months):
        price_forecast(self.database, self.product_id, self.prod_name, months)

