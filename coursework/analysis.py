import pandas as pd
from fbprophet import Prophet
from pandas import DataFrame, to_datetime
import matplotlib.pyplot as plt


def sales_diagram(db, prod_id, prod_name, start_date, end_date):
    db.close()
    db.try_connection()
    df = pd.read_sql("select product_id, amount, date from sales where product_id = %s and date"
                     ">= '%s' and date < '%s' order by date" % (
                        prod_id, start_date, end_date), con=db.engine, parse_dates=True)
    print(df)
    df['date'] = pd.to_datetime(df['date']).dt.date
    try:
        fig = df.plot(kind='bar', x='date', y='amount', title="Sales of %s" % prod_name,
                      xlabel="Date", ylabel="Amount").get_figure()
        plt.xticks(rotation=75)
        plt.show()
        fig.savefig('results/sales-' + prod_name + '-' + start_date + '-' + end_date + '.png')
    except Exception as error:
        print("Error occurred: ", error)


def price_trend(db, prod_id, prod_name):
    db.close()
    db.try_connection()
    df = pd.read_sql("select product_id, date, price::numeric::float8 from prices where product_id = %s"
                     " order by date" % prod_id, con=db.engine,
                     parse_dates=True)
    df['date'] = pd.to_datetime(df['date'])
    del df['product_id']
    df = df.set_index('date')
    df = df.asfreq('D', method='ffill')
    try:
        df.plot(marker='.', markersize=2, linestyle='None', color='0.1', label='Daily',
                title='Prices of %s' % prod_name)
        rolling_mean = df['price'].rolling(window=7, center=True).mean()
        rolling_mean.plot(label='Mean (Rolling)')
        year_roll = df['price'].rolling(window=90, center=True).mean()
        fig = year_roll.plot(label='Trend').get_figure()
        plt.legend(loc="upper left")
        print(rolling_mean)
        plt.show()
        fig.savefig('results/trend-' + prod_name + '-price.png')
    except Exception as error:
        print("Error occurred: ", error)


def price_forecast(db, prod_id, prod_name, months):
    db.close()
    db.try_connection()
    df = pd.read_sql("select product_id, date, price::numeric::float8 from prices where product_id = %s"
                     " order by date" % prod_id, con=db.engine,
                     parse_dates=True)
    df['date'] = pd.to_datetime(df['date'])
    del df['product_id']
    df.columns = ['ds', 'y']
    print(df)
    model = Prophet()
    try:
        model.fit(df)
    except Exception as error:
        print("Error occurred: ", error)
        return
    future = list()
    for i in range(1, months):
        date = '2021-%02d' % i
        future.append([date])
    future = DataFrame(future)
    future.columns = ['ds']
    future['ds'] = to_datetime(future['ds'])
    forecast = model.predict(future)
    print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].head())
    fig = model.plot(forecast)
    plt.title(label="Forecast of prices for %s" % prod_name)
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.show()
    fig.savefig('results/forecast-' + prod_name + '-price.png')
