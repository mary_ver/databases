from models import *


class View:

    @staticmethod
    def intro():
        print("Welcome to the app!")
        print("You can interact with database using one of the following options:")

    @staticmethod
    def get_option():
        return input("Enter the option:")

    @staticmethod
    def outro():
        print("Thanks for using!")

    @staticmethod
    def show_options():
        print("1. Get data\n2. Add data\n3. Update data\n4. Delete data\n5. Query")
        print("To exit - print 0")

    @staticmethod
    def opt_get():
        print("Choose the table you want to get info from:")
        print("1. Account\n2. Bank\n3. Card\n4. Client\n5. Transaction\n6. Card/Account")

    @staticmethod
    def get_id():
        id = input("Enter the id:")
        try:
            int(id)
        except Exception:
            print("Incorrect input")
            return None
        return int(id)

    @staticmethod
    def opt_add():
        print("Choose the table you want to add info to:")
        print("1. Account\n2. Bank\n3. Card\n4. Client\n5. Transaction\n6. Card/Account")

    @staticmethod
    def generate():
        print("Do you want to generate random data?")
        return input("Y/n :")

    @staticmethod
    def get_number_of_rec():
        ans = input("Enter the number of records:")
        try:
            int(ans)
        except Exception:
            print("Incorrect input of parameter")
            return None
        return int(ans)


    @staticmethod
    def return_id(id_, model):
        st_mod1 = str(type(model)).lstrip("<clas 'models.")
        st_mod = st_mod1.strip("'>")
        print("Your %s has an id: %s" % (st_mod, id_))

    @staticmethod
    def opt_upd():
        print("Choose the table you want to modify info in:")
        print("1. Account\n2. Bank\n3. Card\n4. Client\n5. Transaction\n6. Card/Account")

    @staticmethod
    def opt_del():
        print("Choose the table you want to delete info from:")
        print("1. Account\n2. Bank\n3. Card\n4. Client\n5. Transaction\n6. Card/Account")

    @staticmethod
    def opt_query():
        print("Choose one of the following queries to execute:")
        print("1. Find the clients, whose accounts are of a certain type with sum from a range"
              " in bank of a certain owner")
        print("2. Find the bank(-s) whose accounts are most frequent in certain place of transactions")
        print("3. Find all certain clients account in certain bank, opened in certain period of time")
        ans = input("Enter the number:")
        try:
            int(ans)
        except Exception:
            print("Incorrect input")
            return None
        return ans

    @staticmethod
    def query1_get_args():
        owner = input("Enter bank owner:")
        type = input("Enter type:")
        r_start = input("Enter the start of range:")
        r_end = input("Enter the end of range:")
        d = {'owner': owner, 'type': type, 'r_start': r_start, 'r_end': r_end}
        return d

    @staticmethod
    def query2_get_args():
        place = input("Enter place:")
        return  place

    @staticmethod
    def query3_get_args():
        name = input("Enter client's name:")
        bank_id = input("Enter bank id:")
        d_start = input("Enter the start of date range:")
        d_end = input("Enter the end of date range:")
        d = {'name': name, 'bank_id': bank_id, 'd_start': d_start, 'd_end': d_end}
        return d

    @staticmethod
    def upd_info():
        print("----------INFO---------")
        print("> If you want to update\n"
              "only particular fields\n"
              "leave the rest blank")
        print("-----------------------")

    @staticmethod
    def incorrect_opt():
        print("The option you've entered is out of range. Try again")

    @staticmethod
    def incorrect_input(obj):
        print("Incorrect input of %s" % obj)

    @staticmethod
    def print_model(model):
        if isinstance(model, Account):
            acc = model
            print("--------ACCOUNT--------")
            print('ID: %s\nClient_id: %s\nBank_id: %s\nType: %s\nSum: %s\nStart_date: %s' % (
                acc.ID, acc.Client_Id, acc.Bank_Id, acc.Type, acc.Sum, acc.Start_date
            ))
            print("-----------------------")
        elif isinstance(model, Bank):
            bank = model
            print("----------BANK----------")
            print('EDRPOU: %s\nOwner: %s' % (
                bank.EDRPOU, bank.Owner
            ))
            print("------------------------")
        elif isinstance(model, Card):
            card = model
            print("----------CARD----------")
            print('ID: %s\nBank_id: %s\nExp_date: %s' % (
                card.ID, card.Bank_Id, card.Exp_date
            ))
            print("------------------------")
        elif isinstance(model, Client):
            client = model
            print("---------CLIENT---------")
            print('ITN: %s\nFullname: %s\nDate of birth: %s\nPhone: %s\nEmail: %s' % (
                client.ITN, client.Fullname, client.Date_of_birth, client.Phone, client.Email
            ))
            print("------------------------")
        elif isinstance(model, Transaction):
            transaction = model
            print("-------TRANSACTION-------")
            print('ID: %s\nAccount_id: %s\nSum: %s\nDate: %s\nPlace: %s' % (
                transaction.ID, transaction.Account_Id, transaction.Sum, transaction.Date, transaction.Place
            ))
            print("-------------------------")
        elif isinstance(model, CardAccount):
            card_acc = model
            print("------CARD-ACCOUNT-------")
            print('Account_id: %s\nCard_id: %s' % (
                card_acc.Account_Id, card_acc.Card_Id
            ))
            print("-------------------------")
        else:
            print("Wrong model type - out of range")

    @staticmethod
    def get_model(model, id):
        if model == "Account":
            _c_id = input("Enter the client_id:")
            try:
                _c_id = int(_c_id)
            except Exception:
                print("Incorrect input of client_id")
                return None
            _b_id = input("Enter the bank_id:")
            try:
                _b_id = int(_b_id)
            except Exception:
                print("Incorrect input of bank_id")
                return None
            _type = input("Enter the type:")
            _sum = input("Enter the start sum:")
            try:
                float(_sum)
            except Exception:
                print("Incorrect input of sum")
                return None
            _date = input("Enter the start date in format DD.MM.YYYY:")
            return Account(int(id), _c_id, _b_id, _type, _sum, _date)
        elif model == "Bank":
            _owner = input("Enter the owner:")
            return Bank(int(id), _owner)
        elif model == "Card":
            _b_id = input("Enter the bank id:")
            try:
                _b_id = int(_b_id)
            except Exception:
                print("Incorrect input of bank_id")
                return None
            _date = input("Enter the exp_date in format DD.MM.YYYY:")
            return Card(int(id), _b_id, _date)
        elif model == "Client":
            _name = input("Enter the fullname")
            _date = input("Enter the date of birth in format DD.MM.YYYY:")
            _phone = input("Enter the client's phone number:")
            try:
                _phone = int(_phone)
            except Exception:
                print("Incorrect input of client phone number")
                return None
            _email = input("Enter the client's email: ")
            return Client(int(id), _name, _date, _phone, _email)
        elif model == "Transaction":
            _a_id = input("Enter account id:")
            try:
                _a_id = int(_a_id)
            except Exception:
                print("incorrect input of account id")
                return None
            _sum = input("Enter the sum:")
            try:
                float(_sum)
            except Exception:
                print("Incorrect input of sum")
                return None
            _date = input("Enter the date in format DD.MM.YYYY:")
            _place = input("Enter place (optional):")
            return Transaction(int(id), _a_id, _sum, _date, _place)
        elif model == "Card/Account":
            c_id = input("Enter card id:")
            try:
                c_id = int(c_id)
            except Exception:
                print("incorrect input of card id")
                return None
            a_id = input("Enter account id:")
            try:
                a_id = int(a_id)
            except Exception:
                print("incorrect input of account id")
                return None
            return CardAccount(c_id, a_id)

    @staticmethod
    def upd_model(model):
        if isinstance(model, Account):
            acc = model
            _c_id = input("Enter the client_id:")
            if _c_id == '':
                _c_id = acc.Client_Id
            else:
                try:
                    _c_id = int(_c_id)
                except Exception:
                    print("Incorrect input of client_id")
                    return None
            _b_id = input("Enter the bank_id:")
            if _b_id == '':
                _b_id = acc.Bank_Id
            else:
                try:
                    _b_id = int(_b_id)
                except Exception:
                    print("Incorrect input of bank_id")
                    return None
            _type = input("Enter the type:")
            if _type == '':
                _type = acc.Type
            _sum = input("Enter the start sum:")
            if _sum == '':
                _sum = acc.Sum
            else:
                try:
                    float(_sum)
                except Exception:
                    print("Incorrect input of sum")
                    return None
            _date = input("Enter the start date in format DD.MM.YYYY:")
            if _date == '':
                _date = acc.Start_date
            return Account(acc.ID, _c_id, _b_id, _type, _sum, _date)
        elif isinstance(model, Bank):
            bank = model
            _owner = input("Enter the owner:")
            if _owner == '':
                _owner = bank.Owner
            return Bank(bank.EDRPOU, _owner)
        elif isinstance(model, Card):
            card = model
            _b_id = input("Enter the bank id:")
            if _b_id == '':
                _b_id = card.Bank_Id
            else:
                try:
                    _b_id = int(_b_id)
                except Exception:
                    print("Incorrect input of bank_id")
                    return None
            _date = input("Enter the exp_date in format DD.MM.YYYY:")
            if _date == '':
                _date = card.Exp_date
            return Card(card.ID, _b_id, _date)
        elif isinstance(model, Client):
            client = model
            _name = input("Enter the fullname")
            if _name == '':
                _name = client.Fullname
            _date = input("Enter the date of birth in format DD.MM.YYYY:")
            if _date == '':
                _date = client.Date_of_birth
            _phone = input("Enter the client's phone number:")
            if _phone == '':
                _phone = client.Phone
            else:
                try:
                    _phone = int(_phone)
                except Exception:
                    print("Incorrect input of client phone number")
                    return None
            _email = input("Enter the client's email:")
            if _email == '':
                _email = client.Email
            return Client(client.ITN, _name, _date, _phone, _email)
        elif isinstance(model, Transaction):
            trans = model
            _a_id = input("Enter account id:")
            if _a_id == '':
                _a_id = trans.Account_Id
            else:
                try:
                    _a_id = int(_a_id)
                except Exception:
                    print("Incorrect input of account id")
                    return None
            _sum = input("Enter the sum:")
            if _sum == '':
                _sum = trans.Sum
            else:
                try:
                    float(_sum)
                except Exception:
                    print("Incorrect input of sum")
                    return None
            _date = input("Enter the date in format DD.MM.YYYY:")
            if _date == '':
                _date = trans.Date
            _place = input("Enter place (optional):")
            if _place == '':
                _place = trans.Place
            return Transaction(trans.ID, _a_id, _sum, _date, _place)
        elif isinstance(model, CardAccount):
            c_a = model
            new_c_id = input("Enter new card id:")
            if new_c_id == '':
                new_c_id = c_a.Card_Id
            else:
                try:
                    new_c_id = int(new_c_id)
                except Exception:
                    print("Incorrect input of card id")
                    return None
            return CardAccount(new_c_id, c_a.Account_Id)