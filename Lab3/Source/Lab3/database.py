import psycopg2
import math
import time
from models import *
import psycopg2.extensions
from psycopg2.extras import LoggingConnection, LoggingCursor
import logging
from sqlalchemy import *
from sqlalchemy.orm import session, sessionmaker

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class MyLoggingCursor(LoggingCursor):
    def execute(self, query, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).execute(query, vars)

    def callproc(self, procname, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).callproc(procname, vars)


class MyLoggingConnection(LoggingConnection):
    def filter(self, msg, curs):
        return "%s ms" % int((time.time() - curs.timestamp) * 1000)

    def cursor(self, *args, **kwargs):
        kwargs.setdefault('cursor_factory', MyLoggingCursor)
        return LoggingConnection.cursor(self, *args, **kwargs)


DATABASE_URI = 'postgres+psycopg2://postgres:qwerty@localhost:5432/lab1_bank'
engine = create_engine(DATABASE_URI)

Session = sessionmaker(bind=engine)

class Database:

    def __init__(self):
        self.connection = None
        self.cursor = None
        self.session = None

# WORKS
    def connect(self):
        try:
            self.session = Session()

        except (Exception, psycopg2.Error) as error:
            print("Failed connection to PostgreSQL", error)


# WORKS
    def close(self):
        if self.session:
            self.session.close()
            print("\nConnection closed")

    def get_bank(self, bank_id):
        try:
            bank = self.session.query(Bank).filter_by(EDRPOU=int(bank_id)).first()
            return bank
        except (Exception, psycopg2.Error) as error:
            print("Failed getting bank:", error)
            self.session.rollback()

# WORKS
    def add_bank(self, bank):
        try:
            bank.EDRPOU = Sequence('b_ser').next_value()
            self.session.add(bank)
            self.session.commit()
            return bank.EDRPOU
        except (Exception, psycopg2.Error) as error:
            print("Failed adding bank to the database", error)
            self.session.rollback()

# WORKS
    def update_bank(self, bank):
        try:
            self.session.query(Bank).filter(Bank.EDRPOU == int(bank.EDRPOU)).\
                update({Bank.Owner: bank.Owner})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the bank:", error)
            self.session.rollback()

    def del_bank(self, bank):
        try:
            self.session.query(Bank).filter_by(EDRPOU=int(bank.EDRPOU)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting bank in the database:", error)
            self.session.rollback()

    def get_acc(self, account_id):
        try:
            acc = self.session.query(Account).filter_by(ID=int(account_id)).first()
            return acc
        except (Exception, psycopg2.Error) as error:
            print("Failed getting account:", error)
            self.session.rollback()

    def add_account(self, account):
        try:
            account.ID = Sequence('a_ser').next_value()
            self.session.add(account)
            self.session.commit()
            return account.ID
        except (Exception, psycopg2.Error) as error:
            print("Failed adding account to the database:", error)
            self.session.rollback()

    def update_account(self, account):
        try:
            self.session.query(Account).filter(Account.ID == int(account.ID)).\
                update({"Client_Id": account.Client_Id,
                        "Bank_Id": account.Bank_Id,
                        "Type": account.Type,
                        "Sum": account.Sum,
                        "Start_date": account.Start_date})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the account:", error)
            self.session.rollback()

    def del_account(self, account):
        try:
            self.session.query(Account).filter_by(ID=int(account.ID)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting account in the database:", error)
            self.session.rollback()

    def get_client(self, client_id):
        try:
            c = self.session.query(Client).filter_by(ITN=int(client_id)).first()
            return c
        except (Exception, psycopg2.Error) as error:
            print("Failed getting client:", error)
            self.session.rollback()

    def add_client(self, client):
        try:
            client.ITN = Sequence('cl_ser').next_value()
            self.session.add(client)
            self.session.commit()
            return client.ITN
        except (Exception, psycopg2.Error) as error:
            print("Failed adding client to the database:", error)
            self.session.rollback()

    def update_client(self, client):
        try:
            self.session.query(Client).filter(Client.ITN == int(client.ITN)).\
                update({"Fullname": client.Fullname,
                        "Date_of_birth": client.Date_of_birth,
                        "Phone": client.Phone,
                        "Email": client.Email})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the client:", error)
            self.session.rollback()

    def del_client(self, client):
        try:
            self.session.query(Client).filter_by(ITN=int(client.ITN)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting the client in the database:", error)
            self.session.rollback()

    def get_card(self, card_id):
        try:
            c = self.session.query(Card).filter_by(ID=int(card_id)).first()
            return c
        except (Exception, psycopg2.Error) as error:
            print("Failed getting card", error)
            self.session.rollback()

    def add_card(self, card):
        try:
            card.ID = Sequence('c_ser').next_value()
            self.session.add(card)
            self.session.commit()
            return card.ID
        except (Exception, psycopg2.Error) as error:
            print("Failed adding card to the database:", error)
            self.session.rollback()

    def update_card(self, card):
        try:
            self.session.query(Card).filter(Card.ID == int(card.ID)).\
                update({"Bank_Id": card.Bank_Id,
                        "Exp_date": card.Exp_date})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the card:", error)
            self.session.rollback()

    def del_card(self, card):
        try:
            self.session.query(Client).filter_by(ID=int(card.ID)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting card in the database:", error)
            self.session.rollback()

    def get_transaction(self, trans_id):
        try:
            tr = self.session.query(Transaction).filter_by(ID=int(trans_id)).first()
            return tr
        except (Exception, psycopg2.Error) as error:
            print("Failed getting transaction:", error)
            self.session.rollback()

    def add_transaction(self, transaction):
        try:
            transaction.ID = Sequence('t_ser').next_value()
            self.session.add(transaction)
            self.session.commit()
            return transaction.ID
        except (Exception, psycopg2.Error) as error:
            print("Failed adding transaction to the database:", error)
            self.session.rollback()

    def update_transaction(self, transaction):
        try:
            self.session.query(Transaction).filter(Transaction.ID == int(transaction.ID)).\
                update({"Account_Id": transaction.Account_Id,
                        "Sum": transaction.Sum,
                        "Date": transaction.Date,
                        "Place": transaction.Place})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the transaction:", error)
            self.session.rollback()

    def del_transaction(self, transaction):
        try:
            self.session.query(Transaction).filter_by(ID=int(transaction.ID)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting transaction in the database:", error)
            self.session.rollback()

    def get_card_account(self, a_id, c_id):
        try:
            ca = self.session.query(CardAccount).filter_by(Account_Id=int(a_id), Card_Id=int(c_id)).first()
            return ca
        except (Exception, psycopg2.Error) as error:
            print("Failed getting transaction:", error)
            self.session.rollback()

    def add_card_account(self, card_account):
        try:
            statement = card_account_association.insert()\
                .values(Account_Id=card_account.Account_Id, Card_Id=card_account.Card_Id)
            self.session.execute(statement)
            self.session.commit()
            print("Added successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed adding card-account to the database:", error)
            self.session.rollback()

    def update_card_account(self, o_card_account, card_account):
        try:
            self.session.query(CardAccount)\
                .filter(CardAccount.Account_Id == int(o_card_account.Account_Id),
                        CardAccount.Card_Id == int(o_card_account.Card_Id)).\
                update({"Card_Id": card_account.Card_Id})
            self.session.flush()
            self.session.commit()
            print("Updated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed updating the card-account:", error)
            self.session.rollback()

    def del_card_account(self, card_account):
        try:
            self.session.query(CardAccount)\
                .filter(CardAccount.Account_Id == int(card_account.Account_Id),
                        CardAccount.Card_Id == int(card_account.Card_Id)).delete()
            self.session.commit()
            print("Deleted successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed deleting card-account in the database:", error)
            self.session.rollback()

    def generate_b(self, number):
        try:
            self.connection = psycopg2.connect(user="postgres",
                                               password="qwerty",
                                               host="127.0.0.1",
                                               port="5432",
                                               database="lab1_bank")
            self.cursor = self.connection.cursor()
            self.cursor.execute(
                "insert into \"Bank\" (\"Owner\") select chr(trunc(65+random()*25)::int) || "
                "chr(trunc(65+random()*25)::int)"
                " || chr(trunc(65+random()*25)::int) || chr(trunc(65+random()*25)::int) as owner "
                "from generate_series(1, %s)" % number
            )
            self.connection.commit()
            print("Generated successfully")
        except (Exception, psycopg2.Error) as error:
            print("Failed generating:", error)
            self.connection.rollback()
            self.connection.close()
            self.cursor.close()
            self.connect()

    def generate_c(self, number):
        try:
            self.connection = psycopg2.connect(user="postgres",
                                               password="qwerty",
                                               host="127.0.0.1",
                                               port="5432",
                                               database="lab1_bank")
            self.cursor = self.connection.cursor()
            self.cursor.execute(
                "WITH expanded AS ( SELECT RANDOM(), seq, b.\"EDRPOU\" AS bank_id "
                "FROM GENERATE_SERIES(1, %s) seq, \"Bank\" b), "
                "shuffled AS ( SELECT e.* FROM expanded e INNER JOIN ("
                "SELECT ei.seq, MIN(ei.random) FROM expanded ei GROUP BY ei.seq)"
                " em ON (e.seq = em.seq AND e.random = em.min)"
                "ORDER BY e.seq),"
                "dates as (select (timestamp '2000-01-10 00:00:00' +"
                "random() * (timestamp '2021-01-01 00:00:00' - timestamp '2000-01-01 00:00:00'))::date as n_date "
                "from generate_series(1, %s)) insert into \"Card\" (\"Bank_Id\", \"Exp_date\") "
                "select s.bank_id, d.n_date from shuffled as s, dates as d group by random(), s.bank_id, d.n_date"
                " limit %s" % (
                    int(math.sqrt(int(number))) + 1,  int(math.sqrt(int(number))) + 1, int(number)
                )
            )
            self.connection.commit()
        except (Exception, psycopg2.Error) as error:
            print("Failed generating:", error)
            self.connection.rollback()
            self.connection.close()
            self.cursor.close()
            self.connect()

    def query1(self, owner, type, start, end):
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()

        try:
            self.cursor.execute(
                "select * from \"Client\" where \"ITN\" = "
                "(select \"Client_Id\" from \"Account\" "
                "where \"Bank_Id\" = (select \"EDRPOU\" "
                "as b_id from \"Bank\" where \"Owner\" like %s) "
                "and \"Type\" like %s and \"Sum\"::money::numeric::float8 "
                "between %s and %s)" % (str("'%"+owner+"%'"), str("'"+type+"'"), start, end)
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No client to match your query")
            clients = self.cursor.fetchall()
            self.connection.close()
            self.connect()
            return clients
        except (Exception, psycopg2.Error) as error:
            print("Error: ", error)
            self.connection.rollback()
            self.connection.close()
            self.connect()
            return None

    def query2(self, place):
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute(
                "with count_times as (select \"Account_Id\", count(\"Account_Id\") as n from \"Transaction\" "
                "where \"Place\" like %s group by \"Account_Id\"),"
                " sel_max as (select \"Account_Id\", n "
                "from count_times "
                "where n = (select max(n) from count_times)"
                " group by \"Account_Id\", n) "
                "select * from \"Bank\" as c "
                "inner join "
                "(select \"Bank_Id\" from \"Account\" as a "
                "inner join "
                "sel_max as b "
                "on a.\"ID\"=b.\"Account_Id\") as d "
                "on c.\"EDRPOU\"=d.\"Bank_Id\"" % str("'%"+place+"%'")
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No bank to match your query")
            banks = self.cursor.fetchall()
            self.connection.close()
            self.connect()
            return banks
        except (Exception, psycopg2.Error) as error:
            print ("Error: ", error)
            self.connection.close()
            self.connect()
            self.connection.rollback()
            return None

    def query3(self, name, bank_id, d_start, d_end):
        self.connection = psycopg2.connect(connection_factory=MyLoggingConnection,
                                           user="postgres",
                                           password="qwerty",
                                           host="127.0.0.1",
                                           port="5432",
                                           database="lab1_bank")
        self.connection.initialize(logger)
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute(
                "with accs as (select * from \"Account\" as a "
                "where \"Start_date\"::date between timestamp '%s' and timestamp '%s' "
                "and \"Bank_Id\" = %s)"
                "select * from accs as a "
                "inner join "
                "(select \"ITN\" from \"Client\" where \"Fullname\" like %s) as b "
                "on a.\"Client_Id\" = b.\"ITN\"" % (d_start, d_end, int(bank_id), str("'%"+name+"%'"))
            )
            self.connection.commit()
            if self.cursor.rowcount == 0:
                raise Exception("No account to match your query")
            accs = self.cursor.fetchall()
            accounts = []
            for row in accs:
                date = str(row[5]).replace('-', '.')
                self.cursor.execute(
                    "select (select t.\"Sum\" from public.\"Account\" as t where t.\"ID\" = %s)::money::numeric::float8"
                    % row[0]
                )
                self.connection.commit()
                res = self.cursor.fetchall()
                sum1 = str(res[0]).replace('(', '')
                sum2 = sum1.replace(',)', '')
                ac = Account(row[0], row[1], row[2], row[3], sum2, date)
                accounts.append(ac)
            self.connection.close()
            self.connect()
            return accounts
        except (Exception, psycopg2.Error) as error:
            print("Error: ", error)
            self.connection.close()
            self.connect()
            self.connection.rollback()
            return None

