from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.dialects.postgresql import MONEY
from sqlalchemy.orm import relationship, backref

Base = declarative_base()

meta = MetaData()


class Bank(Base):
    __tablename__ = 'Bank'
    EDRPOU = Column(Integer, Sequence('b_ser'), primary_key=True, server_default=Sequence('b_ser').next_value())
    Owner = Column(Text, nullable=False)
    accounts = relationship('Account', cascade="all, delete", passive_deletes=True)
    cards = relationship('Card', cascade="all, delete", passive_deletes=True)

    def __init__(self, b_id, owner):
        self.EDRPOU = int(b_id)
        self.Owner = str(owner)


card_account_association = Table(
    'Card/Account', Base.metadata,
    Column('Card_Id', Integer, ForeignKey('Card.ID')),
    Column('Account_Id', Integer, ForeignKey('Account.ID'))
)


class Account(Base):
    __tablename__ = 'Account'
    ID = Column(Integer, primary_key=True)
    Client_Id = Column(Integer, ForeignKey('Client.ITN', ondelete='CASCADE'))
    Bank_Id = Column(Integer, ForeignKey('Bank.EDRPOU', ondelete='CASCADE'))
    Type = Column(Text, nullable=False)
    Sum = Column(MONEY)
    Start_date = Column(Date, nullable=False)
    cards = relationship("Card", secondary=card_account_association)
    transactions = relationship('Transaction', backref='Account', passive_deletes=True)

    def __init__(self, a_id, cl_id, b_id, a_type, a_sum, date):
        self.ID = int(a_id)
        self.Client_Id = int(cl_id)
        self.Bank_Id = int(b_id)
        self.Type = str(a_type)
        self.Sum = str(a_sum)
        self.Start_date = str(date)


class Card(Base):
    __tablename__ = "Card"
    ID = Column(Integer, primary_key=True)
    Bank_Id = Column(Integer, ForeignKey('Bank.EDRPOU', ondelete='CASCADE'))
    Exp_date = Column(Date, nullable=False)

    def __init__(self, c_id, b_id, exp_date):
        self.ID = int(c_id)
        self.Bank_Id = int(b_id)
        self.Exp_date = str(exp_date)


class Client(Base):
    __tablename__ = 'Client'
    ITN = Column(Integer, primary_key=True)
    Fullname = Column(Text, nullable=False)
    Date_of_birth = Column(Date, nullable=False)
    Phone = Column(Integer)
    Email = Column(Text)
    account = relationship("Account")

    def __init__(self, itn, fullname, date_birth, phone, email):
        self.ITN = int(itn)
        self.Fullname = str(fullname)
        self.Date_of_birth = str(date_birth)
        self.Phone = int(phone)
        self.Email = str(email)


class Transaction(Base):
    __tablename__ = 'Transaction'
    ID = Column(Integer, primary_key=True)
    Account_Id = Column(Integer, ForeignKey('Account.ID', ondelete='CASCADE'))
    Sum = Column(MONEY, nullable=False)
    Date = Column(Date, nullable=False)
    Place = Column(Text)

    def __init__(self, t_id, a_id, t_sum, date, place):
        self.ID = int(t_id)
        self.Account_Id = int(a_id)
        self.Sum = str(t_sum)
        self.Date = str(date)
        self.Place = str(place)


class CardAccount(Base):
    __tablename__ = 'Card/Account'
    __table_args__ = {'extend_existing': True}
    Card_Id = Column(Integer, ForeignKey('Card.ID'), primary_key=True)
    Account_Id = Column(Integer,  ForeignKey('Account.ID'), primary_key=True)

    def __init__(self, c_id, a_id):
        self.Card_Id = int(c_id)
        self.Account_Id = int(a_id)
